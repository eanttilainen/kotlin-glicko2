# README #

## What is this repository for? ###

* Glicko2 Kotlin implementation API
Built as a hobby project for tracking Guilty Gear matches.
Note that the deviation calculation for inactivity differs from the basic algorithm described in 
http://www.glicko.net/glicko/glicko2.pdf

Also contains a simple webapp using FreeMarker

Still as of 2019-10-29 missing a lot of basic functionality that I'm planning on adding, like proper database and authentication

## How do I get set up? ###

* Download the latest stable IntelliJ
* Import the project
* Sync Gradle
* Install Ktor plugin
* IntelliJ should prompt you to install everything that is needed
* Follow the instructions here to run the project from command line / other environments: https://kotlinlang.org/docs/tutorials/command-line.html
* After installing everything required, you can run the server with the command ./gradlew run

The application runs using port 8080 by default. Can be changed in application.conf

## Endpoints:
* `/api/v1/`
	* `matches/add` Adds a new set of matches to the database to be calculated as a single Rating Period
	* `matches/list` lists all the matches in the database
	* `players?name=$name` returns the Player whose name matches param $name
	* `players/list` lists all the players in the database, sorted by glicko2Rating

## Folder Structure ###
* `GlickoRating/`
    * `src/application`: Entry point to the application. Installs ktor features (headers, json, sessions) and handles routing in a declarative way.
    * `src/api`: API end points / routing extensions
    * `src/models`: models
    * `src/repository`: Repository for handling data flow. Interfaces & actual implementations. InMemory temporary implementations that can be easily swapped with real implementations
    * `src/web`: End points that return an html template
    * `resources/application.conf` application and deployment configs
    * `resources/templates` FreeMarker html templates
	* `test/` Unit tests
	
## Libraries and technologies ###
Code is written in Kotlin
Networking done with Ktor using Netty as the http engine
Moshi for JSON parsing
FreeMarker for html templates
	
## Architecture ###
![Architecture flowchart](architecture_flowchart.png)
	
## Example queries ##
Adding a set of matches:
```
curl -X POST \
  http://localhost:8080/api/v1/matches/add \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 1680' \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:8080' \
  -H 'User-Agent: PostmanRuntime/7.17.1' \
  -H 'cache-control: no-cache' \
  -d '[
	{
		"player1":"GG PLAYER (SO)",
		"player2":"BB PLAYER (EL)",
		"player1wins":7,
		"player2wins":6,
		"date":"2019-10-29"
	},
	{
		"player1":"GG PLAYER (SO)",
		"player2":"Test (JO)",
		"player1wins":0,
		"player2wins":7,
		"date":"2019-10-29"
	},
]'
```
Getting a list of players:
```
curl -X GET \
  http://localhost:8080/api/v1/players/list \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Host: localhost:8080' \
  -H 'User-Agent: PostmanRuntime/7.17.1' \
  -H 'cache-control: no-cache'
```

Example response:
```
[
    {
        "matchesPlayed": 7,
        "name": "Test (JO)",
        "glicko2Rating": {
            "deviation": 137.38116856316512,
            "deviationScaled": 0.7908295440258,
            "glicko2Rating": 1728.337984679321,
            "ratingScaled": 1.3144190444463428,
            "volatility": 0.05999977462415979
        }
    },
    {
        "matchesPlayed": 20,
        "name": "GG PLAYER (SO)",
        "glicko2Rating": {
            "deviation": 110.21316237982818,
            "deviationScaled": 0.6344379354322249,
            "glicko2Rating": 1359.649153032813,
            "ratingScaled": -0.8079243863736874,
            "volatility": 0.05999968086936046
        }
    },
    {
        "matchesPlayed": 13,
        "name": "BB PLAYER (EL)",
        "glicko2Rating": {
            "deviation": 104.5941772562217,
            "deviationScaled": 0.6020924583216095,
            "glicko2Rating": 1329.6944509541067,
            "ratingScaled": -0.9803575053672867,
            "volatility": 0.060001935276025625
        }
    }
]
```
## Contact ##

* Discord: Satsu#8102
* Twitter: https://twitter.com/satsuasdf

