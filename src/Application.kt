package com.satsu

import com.ryanharter.ktor.moshi.moshi
import com.satsu.api.matches
import com.satsu.api.players
import com.satsu.repository.InMemoryRepository
import com.satsu.web.webRankings
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.http.*
import freemarker.cache.*
import io.ktor.freemarker.*
import io.ktor.features.*

const val API_VERSION = "/api/v1"
fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(FreeMarker) {
        templateLoader = ClassTemplateLoader(this::class.java.classLoader, "templates")
    }
    install(DefaultHeaders)
    install(ContentNegotiation) {
        moshi()
    }

    install(StatusPages) {
        exception<AuthenticationException> { _ ->
            call.respond(HttpStatusCode.Unauthorized)
        }
        exception<AuthorizationException> { _ ->
            call.respond(HttpStatusCode.Forbidden)
        }
    }

    val db = InMemoryRepository()

    routing {
        matches(db)
        players(db)
        webRankings(db)
    }
}
class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()

