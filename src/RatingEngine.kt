package com.satsu

import com.satsu.models.Rating
import kotlin.math.*

//http://www.glicko.net/glicko/glicko2.pdf

object RatingEngine {

    private const val SET_ABSENCE_C = 100
    const val DEFAULT_RATING = 1500.0
    const val DEFAULT_DEVIATION = 350.0
    const val DEFAULT_VOLATILITY = 0.06
    private const val GLICKO_SCALE = 173.7178
    private const val CONVERGENCE_TOLERANCE = 0.000001
    private const val VOLATILITY_CONSTRAINT = 0.5

    fun scaleRating(rating: Double) = (rating - DEFAULT_RATING) / GLICKO_SCALE
    private fun unscaleRating(rating: Double) = (rating * GLICKO_SCALE) + DEFAULT_RATING
    fun scaleDeviation(deviation: Double) = deviation / GLICKO_SCALE
    private fun unscaleDeviation(deviation: Double) = deviation * GLICKO_SCALE

    fun g(scaledDeviation: Double) =
        1.0 / (sqrt(1.0 + 3.0 * (scaledDeviation * scaledDeviation) / (Math.PI * Math.PI)))

    fun E(rating1: Rating, rating2: Rating) =
        1.0 / (1.0 + exp(-g(rating2.deviationScaled) * (rating1.ratingScaled - rating2.ratingScaled)))

    fun f(x: Double, scaledDeviation: Double, v: Double, delta: Double, a: Double): Double {
        val eX = Math.E.pow(x)
        val temp = scaledDeviation * scaledDeviation + v + eX
        return eX * (delta * delta - temp) / (2.0 * temp * temp) - (x - a) / (VOLATILITY_CONSTRAINT * VOLATILITY_CONSTRAINT)
    }

    private fun absenceRD(deviation: Double): Double{
        return min(deviation+ SET_ABSENCE_C, 350.0)
    }

    fun updateRating(rating: Rating, opponents: List<Rating>, scores: List<Double>) {

        if (opponents.isEmpty()) {
            //+100RD for a set of inactivity, up to RD==350
            val newDeviation = absenceRD(rating.deviation)
            rating.update(rating.rating, newDeviation, rating.volatility)
            return
        }

        var v = 0.0
        var delta = 0.0
        for (i in opponents.indices) {
            val opponent = opponents[i]
            val score = scores[i]

            val g = g(opponent.deviationScaled)
            val E = E(rating, opponent)

            v += g * g * E * (1.0 - E)
            delta += g * (score - E)
        }
        v = 1.0 / v

        val a = ln(rating.volatility * rating.volatility)
        var A = a
        val delta2 = delta * delta
        val deviation2 = rating.deviationScaled * rating.deviationScaled
        var B: Double
        B = if (delta2 > deviation2 + v) {
            ln(delta2 - (deviation2 + v))
        } else {
            var k = 1
            while (f(a - k * VOLATILITY_CONSTRAINT, rating.deviationScaled, v, delta, a) < 0.0) {
                k++
            }
            a - k * VOLATILITY_CONSTRAINT
        }

        var fA = f(A, rating.deviationScaled, v, delta, a)
        var fB = f(B, rating.deviationScaled, v, delta, a)

        while (abs(B - A) > CONVERGENCE_TOLERANCE) {
            val C = A + (A - B) * fA / (fB - fA)
            val fC = f(C, rating.deviationScaled, v, delta, a)
            if (fC * fB < 0.0) {
                A = B
                fA = fB
            } else {
                fA /= 2.0
            }
            B = C
            fB = fC
        }
        val newVolatility = Math.exp(A / 2.0)
        var newDeviation = sqrt(deviation2 + newVolatility * newVolatility)
        newDeviation = 1.0 / Math.sqrt(1.0 / (newDeviation * newDeviation) + 1.0 / v)
        var newRating = 0.0
        for (i in opponents.indices) {
            val opponent = opponents[i]
            val score = scores[i]
            newRating += g(opponent.deviationScaled) * (score - E(rating, opponent))
        }
        newRating *= newDeviation * newDeviation
        newRating += rating.ratingScaled
        newDeviation = unscaleDeviation(newDeviation)
        newRating = unscaleRating(newRating)

        rating.update(newRating, newDeviation, newVolatility)
    }

}