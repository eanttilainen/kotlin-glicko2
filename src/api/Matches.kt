package com.satsu.api


import com.satsu.API_VERSION
import com.satsu.models.MatchSet
import com.satsu.models.Player
import com.satsu.repository.Repository
import io.ktor.application.call
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post

const val MATCHES_ADD = "$API_VERSION/matches/add"
const val MATCHES_LIST = "$API_VERSION/matches/list"

fun Route.matches(db: Repository) {
    post(MATCHES_ADD) {
        val request = call.receive<Array<MatchSet>>()
        request.sortBy { it.date }
        val matchSets = ArrayList<ArrayList<MatchSet>>()
        var date = ""
        request.forEach { matchSet ->
            if (matchSet.date != date) {
                matchSets.add(ArrayList())
                matchSets.last().add(matchSet)
                date = matchSet.date
            } else {
                matchSets.last().add(matchSet)
            }
        }

        lateinit var response: ArrayList<Player>

        matchSets.forEach {
            response = db.addMatches2(it)
        }

        call.respond(response.sortedByDescending { it.rating.rating }.toTypedArray())
    }
    get(MATCHES_LIST) {

        call.respond(db.listMatches().toArray())
    }
}

