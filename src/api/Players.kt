package com.satsu.api

import com.satsu.API_VERSION
import com.satsu.models.Player
import com.satsu.repository.Repository
import io.ktor.application.call
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post

const val PLAYERS_LIST = "$API_VERSION/players/list"
const val PLAYERS_ADD = "$API_VERSION/players/add"
const val PLAYERS_ADD_BULK = "$API_VERSION/players/add_bulk"
const val PLAYERS = "$API_VERSION/players"

fun Route.players(db: Repository) {
    get(PLAYERS_LIST) {
        val playerList: List<Player> = db.getPlayers().sortedByDescending { it.rating.rating }
        call.respond(playerList.toTypedArray())
    }
    get("$PLAYERS{name}") {
        val name = call.parameters["name"] ?: throw IllegalArgumentException("required parameter name missing")
        call.respond(db.getPlayer(name))
    }
    post(PLAYERS_ADD){
        val request = call.receive<Player>()
        val response = db.addPlayer(request)
        call.respond(response)
    }
    post(PLAYERS_ADD_BULK){
        val request = call.receive<Array<Player>>()
        val response = db.addPlayers(request.toList())
        call.respond(response.toArray())
    }
}