package com.satsu.glicko2

import RatingCalculator

class Glicko2Rating {

    var uid: String? = null
        private set // not actually used by the calculation engine but useful to track whose rating is whose
    /**
     * Return the average skill value of the player.
     *
     * @return double
     */
    var rating: Double = 0.toDouble()
    var ratingDeviation: Double = 0.toDouble()
    var volatility: Double = 0.toDouble()
    var numberOfResults = 0
        private set // the number of results from which the rating has been calculated

    // the following variables are used to hold values temporarily whilst running calculations
    private var workingRating: Double = 0.toDouble()
    private var workingRatingDeviation: Double = 0.toDouble()
    private var workingVolatility: Double = 0.toDouble()

    /**
     * Return the average skill value of the player scaled down
     * to the scale used by the algorithm's internal workings.
     *
     * @return double
     */
    /**
     * Set the average skill value, taking in a value in Glicko2 scale.
     *
     * @param double
     */
    var glicko2Rating: Double
        get() = RatingCalculator.convertRatingToGlicko2Scale(this.rating)
        set(rating) {
            this.rating = RatingCalculator.convertRatingToOriginalGlickoScale(rating)
        }

    /**
     * Return the rating deviation of the player scaled down
     * to the scale used by the algorithm's internal workings.
     *
     * @return double
     */
    /**
     * Set the rating deviation, taking in a value in Glicko2 scale.
     *
     * @param double
     */
    var glicko2RatingDeviation: Double
        get() = RatingCalculator.convertRatingDeviationToGlicko2Scale(ratingDeviation)
        set(ratingDeviation) {
            this.ratingDeviation = RatingCalculator.convertRatingDeviationToOriginalGlickoScale(ratingDeviation)
        }

    /**
     *
     * @param uid           An value through which you want to identify the rating (not actually used by the algorithm)
     * @param ratingSystem  An instance of the RatingCalculator object
     */
    constructor(uid: String, ratingSystem: RatingCalculator) {
        this.uid = uid
        this.rating = ratingSystem.defaultRating
        this.ratingDeviation = ratingSystem.defaultRatingDeviation
        this.volatility = ratingSystem.defaultVolatility
    }

    constructor(
        uid: String,
        ratingSystem: RatingCalculator,
        initRating: Double,
        initRatingDeviation: Double,
        initVolatility: Double
    ) {
        this.uid = uid
        this.rating = initRating
        this.ratingDeviation = initRatingDeviation
        this.volatility = initVolatility
    }

    /**
     * Used by the calculation engine, to move interim calculations into their "proper" places.
     *
     */
    fun finaliseRating() {
        this.glicko2Rating = workingRating
        this.glicko2RatingDeviation = workingRatingDeviation
        this.volatility = workingVolatility

        this.setWorkingRatingDeviation(0.0)
        this.setWorkingRating(0.0)
        this.setWorkingVolatility(0.0)
    }

    /**
     * Returns a formatted rating for inspection
     *
     * @return {ratingUid} / {ratingDeviation} / {volatility} / {numberOfResults}
     */
    override fun toString(): String {
        return uid + " / " +
                rating + " / " +
                ratingDeviation + " / " +
                volatility + " / " +
                numberOfResults
    }

    fun incrementNumberOfResults(increment: Int) {
        this.numberOfResults = numberOfResults + increment
    }

    fun setWorkingVolatility(workingVolatility: Double) {
        this.workingVolatility = workingVolatility
    }

    fun setWorkingRating(workingRating: Double) {
        this.workingRating = workingRating
    }

    fun setWorkingRatingDeviation(workingRatingDeviation: Double) {
        this.workingRatingDeviation = workingRatingDeviation
    }
}