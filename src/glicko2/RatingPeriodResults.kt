package com.satsu.glicko2

import java.util.ArrayList
import java.util.HashSet


class RatingPeriodResults {
    private val results = ArrayList<Result>()
    private val participants = HashSet<Glicko2Rating>()


    /**
     * Add a result to the set.
     *
     * @param winner
     * @param loser
     */
    fun addResult(winner: Glicko2Rating, loser: Glicko2Rating) {
        val result = Result(winner, loser)

        results.add(result)
    }


    /**
     * Record a draw between two players and add to the set.
     *
     * @param player1
     * @param player2
     */
    fun addDraw(player1: Glicko2Rating, player2: Glicko2Rating) {
        val result = Result(player1, player2, true)

        results.add(result)
    }


    /**
     * Get a list of the results for a given player.
     *
     * @param player
     * @return List of results
     */
    fun getResults(player: Glicko2Rating): List<Result> {
        val filteredResults = ArrayList<Result>()

        for (result in results) {
            if (result.participated(player)) {
                filteredResults.add(result)
            }
        }

        return filteredResults
    }


    /**
     * Get all the participants whose results are being tracked.
     *
     * @return set of all participants covered by the resultset.
     */
    fun getParticipants(): Set<Glicko2Rating> {
        // Run through the results and make sure all players have been pushed into the participants set.
        for (result in results) {
            participants.add(result.winner!!)
            participants.add(result.loser!!)
        }

        return participants
    }


    /**
     * Add a participant to the rating period, e.g. so that their rating will
     * still be calculated even if they don't actually compete.
     *
     * @param glicko2Rating
     */
    fun addParticipants(glicko2Rating: Glicko2Rating) {
        participants.add(glicko2Rating)
    }


    /**
     * Clear the resultset.
     */
    fun clear() {
        results.clear()
    }
}