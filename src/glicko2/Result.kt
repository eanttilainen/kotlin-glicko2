package com.satsu.glicko2

class Result {

    private var isDraw = false
    var winner: Glicko2Rating? = null
        private set
    var loser: Glicko2Rating? = null
        private set


    /**
     * Record a new result from a match between two players.
     *
     * @param winner
     * @param loser
     */
    constructor(winner: Glicko2Rating, loser: Glicko2Rating) {
        require(validPlayers(winner, loser))

        this.winner = winner
        this.loser = loser
    }


    /**
     * Record a draw between two players.
     *
     * @param player1
     * @param player2
     * @param isDraw (must be set to "true")
     */
    constructor(player1: Glicko2Rating, player2: Glicko2Rating, isDraw: Boolean) {
        require(!(!isDraw || !validPlayers(player1, player2)))

        this.winner = player1
        this.loser = player2
        this.isDraw = true
    }


    /**
     * Check that we're not doing anything silly like recording a match with only one player.
     *
     * @param player1
     * @param player2
     * @return
     */
    private fun validPlayers(player1: Glicko2Rating, player2: Glicko2Rating): Boolean {
        return player1 != player2
    }


    /**
     * Test whether a particular player participated in the match represented by this result.
     *
     * @param player
     * @return boolean (true if player participated in the match)
     */
    fun participated(player: Glicko2Rating): Boolean {
        return winner == player || loser == player
    }


    /**
     * Returns the "score" for a match.
     *
     * @param player
     * @return 1 for a win, 0.5 for a draw and 0 for a loss
     * @throws IllegalArgumentException
     */
    @Throws(IllegalArgumentException::class)
    fun getScore(player: Glicko2Rating): Double {
        var score: Double

        if (winner == player) {
            score = POINTS_FOR_WIN
        } else if (loser == player) {
            score = POINTS_FOR_LOSS
        } else {
            throw IllegalArgumentException("Player " + player.uid + " did not participate in match")
        }

        if (isDraw) {
            score = POINTS_FOR_DRAW
        }

        return score
    }


    /**
     * Given a particular player, returns the opponent.
     *
     * @param player
     * @return opponent
     */
    fun getOpponent(player: Glicko2Rating): Glicko2Rating? {
        val opponent: Glicko2Rating?

        if (winner == player) {
            opponent = loser
        } else if (loser == player) {
            opponent = winner
        } else {
            throw IllegalArgumentException("Player " + player.uid + " did not participate in match")
        }

        return opponent
    }

    companion object {
        private const val POINTS_FOR_WIN = 1.0
        private const val POINTS_FOR_LOSS = 0.0
        private const val POINTS_FOR_DRAW = 0.5
    }
}