package com.satsu.models

data class Match(val player1: String, val player2: String, val score: Double, val date: String) {
    var id: Int? = null
}