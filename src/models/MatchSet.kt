package com.satsu.models

data class MatchSet(val player1: String, val player2: String, val player1wins: Int, val player2wins: Int, val date: String)