package com.satsu.models

data class Player(
    var name: String,
    var rating: Rating = Rating.newInstance(),
    var matchesPlayed: Int = 0,
    var wins: Int = 0,
    var losses: Int = 0,
    var lastPlayed: String = "Never"
)