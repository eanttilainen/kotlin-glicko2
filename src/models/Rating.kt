package com.satsu.models

import com.satsu.RatingEngine

data class Rating(
    var rating: Double,
    var deviation: Double,
    var volatility: Double
) {
    var ratingScaled: Double = 0.0
    var deviationScaled: Double = 0.0

    fun update(rating: Double, deviation: Double, volatility: Double) {
        this.rating = rating
        this.deviation = deviation
        this.volatility = volatility
        this.ratingScaled = RatingEngine.scaleRating(rating)
        this.deviationScaled = RatingEngine.scaleDeviation(deviation)
    }

    companion object {
        fun newInstance(rating: Double, deviation: Double, volatility: Double): Rating {
            val obj = Rating(rating, deviation, volatility)
            obj.update(rating, deviation, volatility)
            return obj
        }

        fun newInstance(): Rating {
            val obj = Rating(
                RatingEngine.DEFAULT_RATING,
                RatingEngine.DEFAULT_DEVIATION,
                RatingEngine.DEFAULT_VOLATILITY
            )
            obj.update(
                RatingEngine.DEFAULT_RATING,
                RatingEngine.DEFAULT_DEVIATION,
                RatingEngine.DEFAULT_VOLATILITY
            )
            return obj
        }
    }

}