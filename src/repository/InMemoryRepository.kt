package com.satsu.repository

import RatingCalculator
import com.satsu.RatingEngine
import com.satsu.glicko2.Glicko2Rating
import com.satsu.glicko2.RatingPeriodResults
import com.satsu.models.Match
import com.satsu.models.MatchSet
import com.satsu.models.Player
import com.satsu.models.Rating
import java.util.concurrent.atomic.AtomicInteger

class InMemoryRepository : Repository {

    private val matchIdCounter = AtomicInteger()

    private val matches = ArrayList<Match>()
    private val players = ArrayList<Player>()

    override suspend fun listMatches() = matches

    //broken, don't use
    override suspend fun addMatches(matchSetList: List<MatchSet>): ArrayList<Player> {
        val glickoMatchList = arrayListOf<Match>()
        val newMatches = ArrayList<Match>()
        matchSetList.forEach { matchSet ->
            repeat(matchSet.player1wins) {
                glickoMatchList.add(Match(matchSet.player1, matchSet.player2, 1.0, matchSet.date))
            }
            repeat(matchSet.player2wins) {
                glickoMatchList.add(Match(matchSet.player1, matchSet.player2, 0.0, matchSet.date))
            }
        }
        glickoMatchList.forEach { match ->
            match.id = matchIdCounter.incrementAndGet()
            newMatches.add(match)
            if (players.find { it.name == match.player1 } == null) players.add(Player(match.player1))
            if (players.find { it.name == match.player2 } == null) players.add(Player(match.player2))
        }

        val temporaryRating = mutableMapOf<String, Rating>()
        players.forEach { player ->
            val opponents = mutableListOf<String>()
            val scores = mutableListOf<Double>()
            opponents.addAll(newMatches.filter { it.player1 == player.name }.map { it.player2 })
            scores.addAll(newMatches.filter { it.player1 == player.name }.map { it.score })
            opponents.addAll(newMatches.filter { it.player2 == player.name }.map { it.player1 })
            scores.addAll(newMatches.filter { it.player2 == player.name }.map { 1 - it.score })

            val opponentRatings = mutableListOf<Rating>()

            opponents.forEach { opponent ->
                opponentRatings.add(players.find { it.name == opponent }!!.rating)
            }

            if (scores.isNotEmpty()) println(player.name + " scores: " + scores)
            if (scores.isNotEmpty()) println(player.name + " old rating: " + player.rating)
            val tempRating = Rating.newInstance(player.rating.rating, player.rating.deviation, player.rating.volatility)
            RatingEngine.updateRating(tempRating, opponentRatings, scores)
            if (scores.isNotEmpty()) println(player.name + " new rating: " + tempRating)
            player.matchesPlayed += opponents.size
            temporaryRating[player.name] = tempRating
            val debug = tempRating.rating - player.rating.rating
            if (scores.isNotEmpty()) println(player.name + " rating change: " + debug)
        }
        players.forEach { player ->
            temporaryRating[player.name]?.let { player.rating = it }
        }
        matches.addAll(newMatches)
        return players
    }

    private val ratingCalculator = RatingCalculator(0.06, 0.5)
    override suspend fun addMatches2(matchSetList: List<MatchSet>): ArrayList<Player> {
        val results = RatingPeriodResults()
        val glickoMatchList = arrayListOf<Match>()
        val newMatches = ArrayList<Match>()
        val glickoPlayerList = arrayListOf<Glicko2Rating>()

        matchSetList.forEach { matchSet ->
            repeat(matchSet.player1wins) {
                glickoMatchList.add(Match(matchSet.player1, matchSet.player2, 1.0, matchSet.date))
            }
            repeat(matchSet.player2wins) {
                glickoMatchList.add(Match(matchSet.player1, matchSet.player2, 0.0, matchSet.date))
            }
        }
        glickoMatchList.forEach { match ->
            match.id = matchIdCounter.incrementAndGet()
            newMatches.add(match)
            if (players.find { it.name == match.player1 } == null) players.add(Player(match.player1))
            if (players.find { it.name == match.player2 } == null) players.add(Player(match.player2))
        }

        players.forEach {
            val glicko2Player = Glicko2Rating(it.name, ratingCalculator)
            glicko2Player.rating = it.rating.rating
            glicko2Player.ratingDeviation = it.rating.deviation
            glicko2Player.volatility = it.rating.volatility
            glickoPlayerList.add(glicko2Player)
            results.addParticipants(glicko2Player)
        }

        glickoMatchList.forEach { match ->
            if (match.score == 1.0)
                results.addResult(
                    glickoPlayerList.find { it.uid == match.player1 }!!,
                    glickoPlayerList.find { it.uid == match.player2 }!!
                )
            else
                results.addResult(
                    glickoPlayerList.find { it.uid == match.player2 }!!,
                    glickoPlayerList.find { it.uid == match.player1 }!!
                )
        }
        ratingCalculator.updateRatings(results)

        glickoPlayerList.forEach { glickoPlayer ->
            val player = players.find { it.name == glickoPlayer.uid }!!
            player.rating.rating = glickoPlayer.rating
            player.rating.deviation = glickoPlayer.ratingDeviation
            player.rating.volatility = glickoPlayer.volatility
            player.matchesPlayed += glickoPlayer.numberOfResults
        }

        matches.addAll(newMatches)
        players.forEach { player ->
            val playedMatches = matches.filter { match -> (match.player1 == player.name || match.player2 == player.name) }
            if (playedMatches.isEmpty()) player.lastPlayed = "Never"
            else player.lastPlayed = playedMatches.last().date
        }
        return players
    }

    override suspend fun getPlayers() = players

    override suspend fun getPlayer(name: String): Player {
        return players.find { it.name == name } ?: throw IllegalArgumentException("Player name $name doesn't exist")
    }

    override suspend fun addPlayer(player: Player): Player {
        val name = player.name
        require(players.find { it.name == name } == null) { "Player $name already exists" }
        players.add(player)
        return player
    }

    override suspend fun addPlayers(playerList: List<Player>): ArrayList<Player> {
        val newPlayers = arrayListOf<Player>()
        playerList.forEach { player ->
            if (players.find { it.name == player.name } == null) {
                players.add(player)
                newPlayers.add(player)
            } else {
                println("adding player: " + player.name + " failed")
            }
        }
        return newPlayers
    }

}