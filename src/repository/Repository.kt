package com.satsu.repository

import com.satsu.models.Match
import com.satsu.models.MatchSet
import com.satsu.models.Player

interface Repository {
    suspend fun getPlayers(): ArrayList<Player>
    suspend fun getPlayer(name: String): Player
    suspend fun addPlayer(player: Player): Player
    suspend fun addPlayers(playerList: List<Player>): ArrayList<Player>
    suspend fun addMatches(matchSetList: List<MatchSet>): ArrayList<Player>
    suspend fun addMatches2(matchSetList: List<MatchSet>): ArrayList<Player>
    suspend fun listMatches(): ArrayList<Match>
}