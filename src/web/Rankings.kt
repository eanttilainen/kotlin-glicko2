package com.satsu.web

import com.satsu.models.Player
import com.satsu.repository.Repository
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import kotlin.math.roundToInt

const val RANKINGS = "/rankings"

fun Route.webRankings(db: Repository) {
        get(RANKINGS) {
            val playerList: List<Player> = db.getPlayers().sortedByDescending { it.rating.rating }
            val displayableList = playerList.map { it.name+" "+ it.rating.rating.roundToInt() +"±"+it.rating.deviation.roundToInt()+" Matches played: "+it.matchesPlayed+" Last played: "+it.lastPlayed}
            call.respond(FreeMarkerContent("rankings.ftl", mapOf("players" to displayableList)))
        }
    }