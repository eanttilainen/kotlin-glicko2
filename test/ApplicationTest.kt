package com.satsu

import RatingCalculator
import com.satsu.glicko2.Glicko2Rating
import com.satsu.glicko2.RatingPeriodResults
import com.satsu.models.Rating
import com.satsu.web.RANKINGS
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import freemarker.cache.*
import io.ktor.freemarker.*
import io.ktor.features.*
import kotlin.test.*
import io.ktor.server.testing.*

class ApplicationTest {

    @Test
    fun testGlicko2(){
        val ratingCalculator = RatingCalculator(0.06, 0.5)
        val results = RatingPeriodResults()
        val player1 = Glicko2Rating("player1", ratingCalculator)
        val player2 = Glicko2Rating("player2", ratingCalculator)
        val player3 = Glicko2Rating("player3", ratingCalculator)
        val player4 = Glicko2Rating("player4", ratingCalculator)
        val player5 = Glicko2Rating("player5", ratingCalculator)

        player1.rating = 1500.0
        player2.rating = 1400.0
        player3.rating = 1550.0
        player4.rating = 1700.0

        player1.ratingDeviation = 200.0
        player2.ratingDeviation = 30.0
        player3.ratingDeviation = 100.0
        player4.ratingDeviation = 300.0


        results.addParticipants(player5)

        println("player1.glickorating expected 0 +-epsilon " + player1.glicko2Rating)
        println("player1.glicko2ratingdeviation expected 1.1513+-e "+player1.glicko2RatingDeviation)

        results.addResult(player1, player2)
        results.addResult(player3, player1)
        results.addResult(player4, player1)
        ratingCalculator.updateRatings(results)

        println(player1)
        println(player2)
        println(player3)
        println(player4)
        println(player5)
    }

    @Test
    fun testRoot() {
        withTestApplication({ module(testing = true) }) {
            handleRequest(HttpMethod.Get, RANKINGS).apply {
                assertEquals(HttpStatusCode.OK, response.status())

                val player1 = Rating.newInstance(1500.0, 200.0, 0.06)
                val player2 = Rating.newInstance(1400.0, 30.0, 0.06)
                val player3 = Rating.newInstance(1550.0, 100.0, 0.06)
                val player4 = Rating.newInstance(1700.0, 300.0, 0.06)
                val player5 = Rating.newInstance(1500.0, 350.0, 0.06)


                println("player1 ratingScaled: "+player1.ratingScaled)
                println("player1 ratingDeviation: "+player1.deviation)
                println("player1 ratingDeviationScaled: "+player1.deviationScaled)

                println("player1 scaled rating: "+player1.ratingScaled)
                println("player1 scaled deviation: "+player1.deviationScaled)
                println("g: "+RatingEngine.g(player2.deviationScaled))
                println("E: "+RatingEngine.E(player1, player2))

                val opponents = listOf(player2, player3, player4)
                val scores = listOf(1.0, 1.0, 1.0)

                val opponents2 = listOf(player1, player3, player4)
                val scores2 = listOf(0.0, 1.0, 1.0)

                val opponents3 = listOf(player1, player2, player4)
                val scores3 = listOf(0.0, 0.0, 1.0)

                val opponents4 = listOf(player1, player2, player3)
                val scores4 = listOf(0.0, 0.0, 0.0)


                val tempRating1 = Rating(player1.rating, player1.deviation, player1.volatility)
                val tempRating2 = Rating(player2.rating, player2.deviation, player2.volatility)
                val tempRating3 = Rating(player3.rating, player3.deviation, player3.volatility)
                val tempRating4 = Rating(player4.rating, player4.deviation, player4.volatility)

                RatingEngine.updateRating(tempRating1, opponents, scores)
                RatingEngine.updateRating(tempRating2, opponents2, scores2)
                RatingEngine.updateRating(tempRating3, opponents3, scores3)
                RatingEngine.updateRating(tempRating4, opponents4, scores4)
                println("player1 new rating: "+tempRating1)
                println("player2 new rating: "+tempRating2)
                println("player3 new rating: "+tempRating3)
                println("player4 new rating: "+tempRating4)
                println("player1 new rating: "+player1.rating)
                println("player2 new rating: "+player2.rating)
                println("player3 new rating: "+player3.rating)
                println("player4 new rating: "+player4.rating)

            }
        }
    }
}
